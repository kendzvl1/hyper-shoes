<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::create([
    		'name' 				=> 'Administrator',
	        'email' 			=> 'admin@suntech.edu.vn',
	        'phone'				=> '0942334333',
	        'address'			=> 'Hà Nội',
	        'gender'			=> 1,
	        'avatar'			=> '',
	        'password' 			=> bcrypt(1),
    	]);

        factory(App\Models\User::class, 49)->create();
    }
}
