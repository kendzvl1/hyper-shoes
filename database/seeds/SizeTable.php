<?php

use Illuminate\Database\Seeder;
use App\Models\Size;
class SizeTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for( $i=35 ;$i<50;$i++){
            Size::create(['size'=>$i]);
        }
    }
}
