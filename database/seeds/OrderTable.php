<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Size;
class OrderTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach(Product::all() as $product){
            $sizes = Size::inRandomOrder()->take(5)->get();
            foreach($sizes as $size){
                $product->sizes()->attach($size->id);
            }
            
        }
    }
}
