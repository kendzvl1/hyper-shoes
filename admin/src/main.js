import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import axios from 'axios';
import VueHtmlToPaper from 'vue-html-to-paper';

Vue.config.productionTip = false
Vue.use(axios);
Vue.use(VueHtmlToPaper, options);

axios.defaults.baseURL = 'http://hypershoes.test/api/v1/';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
axios.defaults.headers.common["Authorization"] = "Bearer " + localStorage.getItem('access_token');
Vue.prototype.$axios = axios

const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    'https://unpkg.com/kidlat-css/css/kidlat.css'
  ]
}


new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
