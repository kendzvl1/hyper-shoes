<?php

use Illuminate\Support\Facades\Route;


Auth::routes(['verify'=>true]);



Route::middleware('verified')->get('/confirm', 'HomeController@confirm')->name('confirm');

Route::middleware('verified')->prefix('user')->group(function(){
	Route::get('/', 'HomeController@user')->name('user');
	Route::post('/','HomeController@userpost');
	Route::get('/orders','HomeController@orders')->name('orders');
	
	Route::get('orders/view/{id}','HomeController@orderview');
	
});

Route::namespace('Frontend')->group(function() {
	Route::prefix('cart')->group(function() {
		Route::get('/', 'CartController@index')->name('cart.index');
		Route::get('store', 'CartController@store')->name('cart.store');
		Route::put('update', 'CartController@update')->name('cart.update');
		Route::get('delete/{id}', 'CartController@destroy')->name('cart.delete');
		Route::post('paymoney','CartController@paymoney')->name('cart.paymoney');
	});


	Route::get('search','SearchController@indexSearch');
	Route::get('search2','SearchController@categorySearch');
	Route::get('/', 'HomeController@index');
	Route::get('{slug}/product', 'ProductController@show');
	Route::get('{slug}/post', 'PostController@show');
	Route::get('{slug}', 'CategoryController@index');
});


