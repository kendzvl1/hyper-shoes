<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Services\PostService;
class news extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $list;
    public function __construct(PostService $postSer)
    {
        //
        $this->list = $postSer->getPost();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.news');
    }
}
