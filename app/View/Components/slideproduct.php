<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Services\ProductService;

class slideproduct extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $sale;
    public $limit;
    public function __construct($limit,ProductService $productService)
    {
        
        $this->sale = $productService->productSale($limit);
       
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        
        return view('components.slideproduct');
    }
}
