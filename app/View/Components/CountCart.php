<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Session;
class CountCart extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $total;
    public function __construct()
    {
        $cart = Session::get('carts') ?? collect();
        $total = 0;
        foreach($cart as $product){
            $total += $product->qty;
        }
        $this->total = $total;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.count-cart');
    }
}
