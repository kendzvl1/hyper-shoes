<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function __construct()
    {
        auth()->setDefaultDriver('api');
    }

    public function login(Request $request){

        $credentials = $request->only('email', 'password');


        if ($token = $this->guard()->attempt($credentials)) {
            
           if(Auth::user()->role === 'admin'){
            return $this->respondWithToken($token);
           }
            
        }

        return response()->json(['error' => 'Unauthorized'], 401);

    }

    public function logout(){
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh(){
        return $this->respondWithToken($this->guard()->refresh());
    }

    public function me(){
        return response()->json($this->guard()->user());
    }


    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    public function guard()
    {
        return Auth::guard();
    }
}
