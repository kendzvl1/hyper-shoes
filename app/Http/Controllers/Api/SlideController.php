<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slide;
use DB;
class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slide = DB::table('Slides')->join('Categories','Slides.category_id','=','Categories.id')->select('Slides.id','Categories.name as category','Slides.image','Slides.slot')->get();
      
        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$slide
        ]);
    }




    public function store(Request $request)
    {
        $slide = new Slide;
        $slide->category_id = $request->category_id;
        $photoUrl ="";
        if($request->image != null){
        $filename = rand(0,100000).$request->file('image')->getClientOriginalName();
        $path = $request->file('image')->move(public_path("/images"),$filename);
        $photoUrl = url('/images/'.$filename);
        }
        

        $slide->image = $photoUrl;
        $slide->slot = $request->slot;

        $slide->save();

        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$slide
        ]);
        
    }


    public function show($id)
    {
        $slide = Slide::find($id);
        if($slide != null){
            return \response()->json([
                'status'=>true,
                'code'=>200,
                'data'=>$slide
            ]);
        }else{
            return \response()->json([
                'status'=>false,
                'code'=>200,
                'message'=>'Không tồn tại slide'
            ]);
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        
        if($slide != null){
            $slide->delete();
            return   \response()->json([
                'status'=>true,
                'code' =>200,
            ]);
        }else{
            return   \response()->json([
                'status'=>false,
                'code' =>200,
                'message' => "Không tồn tại "
            ]);
        }

    }
}
