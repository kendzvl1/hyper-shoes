<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductImage;
class MultiImageController extends Controller
{
    public function getAll($id){
        
        $arrImage = ProductImage::where('product_id','=',$id)->get();

        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$arrImage
      ]);

    }

    public function  createImage(Request $request){

      
        $files=$request->file('image');
       
        $temp=null;
        foreach($files as $file){
            $fileName = rand(0,100000).$file->getClientOriginalName();
            
            $path = $file->move(public_path("/images"),$fileName);
            $photoUrl = url('/images/'.$fileName);
            
            $temp = new ProductImage();
            $temp->product_id=$request->product_id;
            $temp->image=$photoUrl;    
            
           $temp->save();
        }
        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$temp
      ]);
    
    }

    public function destroyImage($id){
        
        ProductImage::destroy($id);
        return \response()->json([
            'status'=>true,
            'code'=>200,
            
        ]);
    }
}
