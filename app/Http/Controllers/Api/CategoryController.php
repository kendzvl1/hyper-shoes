<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Validator;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return \response()->json([
            'status' => true,
            'code' => 200,
            'data' => Category::where('delete','=',0)->with('products')->paginate(10)
        ]);
    }

    public function getAll(){
        return \response()->json([
            'status' => true,
            'code' => 200,
            'data' => Category::where('delete','=',0)->select('id as value','name as text')->get()
        ]);
    }

    public function show($id){
       
            $cate =  Category::find($id);    

            if($cate != null ){
                return \response()->json([
                    'status'=>true,
                    'code' =>200,
                    'data' => $cate
                ]);

            }  else{
                return   \response()->json([
                    'status'=>false,
                    'code' =>200,
                    'message' => "Không tồn tại danh mục"
                ]);

            }
          
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        try{
            $cate =  Category::create($request->all());    
            return \response()->json([
                'status'=>true,
                'code' =>200,
                'data' => $cate
            ]);
        }catch(\Exception $e){
            $this->systemErrors($e);
        }
    
        
        
    }
    private function systemErrors($e){
        return \response()->json([
            'status'=>false,
            'code'=> '',
            'message' => $e->getMessage()
        ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'slug' => 'required',
            'name' => 'required'
        ];

    
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return response()->json(
                [
                    'status'=>false,
                    'message'=>$validator->errors(),
                ]
                ,200);
        }


     
            $cate =  Category::find($id);  
            if($cate != null ){
                

               if( $this->checkCate($cate,$request->slug) ){

                $cate->update($request->all());
                return \response()->json([
                    'status'=>true,
                    'code' =>200,
                    'data' => $cate
                ]);

               }else{
                return   \response()->json([
                    'status'=>false,
                    'code' =>200,
                    'message' => "Slug đã bị trùng"
                ]);
               }
               

            }  else{
                return   \response()->json([
                    'status'=>false,
                    'code' =>200,
                    'message' => "Không tồn tại danh mục"
                ]);

            }
          
       
    }
    public function checkCate($category,$slug){
       
       $temp = Category::where('id','<>',$category->id)->get();
      
       foreach($temp as $ct){
           
            if($ct->slug === $slug){
                return false;
            }
       }
       return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cate = Category::find($id);
        if($cate != null){
            $cate->delete = 1;
            $cate->save();
            return   \response()->json([
                'status'=>true,
                'code' =>200,
                'data' => $cate
            ]);
        }else{
            return   \response()->json([
                'status'=>false,
                'code' =>200,
                'message' => "Không tồn tại danh mục"
            ]);
        }
   
        
    }
}
