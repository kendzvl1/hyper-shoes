<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use DB;
use Carbon\Carbon;

class GuaranteeController extends Controller
{
    public function getAll(Request $request){
        $user = User::where('email','=',$request->email)->first();
        if($user!= null){
           $gua =  DB::table('guarantee')->where('email_user','=',$request->email)->join('guarantee_type','guarantee.id_type','=','guarantee_type.id')
           
           ->select('guarantee.id','guarantee.product_name','guarantee_type.name as nametype','guarantee.time_to_done','guarantee.created_at')->get(); 
        //    $gua['phone']=$user->phone;
        //    $gua['email']=$user->email;
        //    $gua['username']=$user->name;
           
         $data=[];
           foreach($gua as $temp){
            
               $temp->phone = $user->phone;
           
               $temp->username = $user->name;
               array_push($data,$temp);
           }
          // dd($data);
           
           
           return \response()->json([
            'status'=>true,
            'data'=>$data
      ]); 

        }else{
            return \response()->json([
                'status'=>false,
                'message'=>'Record not found'
          ]); 
        }
    }

    public function getType(){
        $list= DB::table('guarantee_type')->select('id as value','name as text')->get();
        return \response()->json([
            'status'=>true,
            'data'=>$list
      ]);
    }

    public function createType(Request $request){
       $t = DB::table('guarantee_type')->insert(
            ['name' => $request->name, 'time' => $request->time]
        );
        return \response()->json([
            'status'=>true,
            'data'=>$t
      ]);
       
    }

    public function addGua(Request $request){
        
        $mutable = Carbon::now();
        $time = DB::table('guarantee_type')->where('id','=',$request->type)->select('time')->get();
        foreach($time as $c){
            $timeAfter = $mutable->add($c->time,'day');
            DB::table('guarantee')->insert([
                ['product_name'=>$request->productName,'email_user'=>$request->email,'id_type'=>$request->type,'created_at'=>Carbon::now(),'time_to_done'=>$timeAfter]
            ]);
        }
        
        return \response()->json([
            'status'=>true
            
      ]);

    }

    public function statistical(Request $request){
        $begin = $request->begin;
        $after = $request->after;
        
        $data = DB::table('orders')->where('status','=',1)->whereBetween('created_at',[$begin,$after])->get();
        // $list = DB::table('orders')->whereBetween('orders.created_at',[$begin,$after])->join('users','orders.user_id','users.id')
        // ->select('orders.id','users.name',);
        
        $soDonhang = count($data);
        $tongThuNhap = 0;

        foreach($data as $order){
           $data2 = DB::table('order_product')->where('order_id','=',$order->id)->get();
           if($data2 != null){ 
               foreach($data2 as $temp){
                   
                   $tongThuNhap = $tongThuNhap + $temp->total;
                  
               }
           }
        }
        $bien['orders']=$soDonhang;
        $bien['total']=$tongThuNhap;
        $bien['begin']=$begin;
        $bien['after']=$after;
        
        return \response()->json([
            'status'=>true,
            'data'=>[
                
                $bien
                
            ],
      
      ]); 
        
    }

}
