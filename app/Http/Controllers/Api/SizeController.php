<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Size;
use App\Models\Product;

class SizeController extends Controller
{
    public function getAll(){

        return \response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>Size::select('id as value','size as text')->get()
      ]);
    }

    public function getOne($id){
        $product = Product::find($id);
       $sizes = $product->sizes()->select('size')->get();

       return \response()->json([
        'status'=>true,
        'code'=>200,
        'data'=>$sizes
  ]);

       

    }



    public function addSize(Request $request,$id){
  //      dd($id);
        $product = Product::find($id);
        $arrSize = $product->sizes->pluck('id')->toArray();
        if($product != null){

            foreach($request->sizes as $size){
            if( $this->checkInArray($size,$arrSize) )
            {
                $product->sizes()->attach($size);
            }
            }
            return \response()->json([
                'status'=>true,
                'code'=>200,
                'data'=>[
                    $product->sizes->pluck('id')
                ]
            ]);

        }else{
            return \response()->json([
                'status'=>false,
                'code'=>200,
                'message'=>'Không tồn lại sản phẩm '
          ]);
        }

    }

    public function checkInArray($size,$arr){
       
        for($i=0;$i<count($arr);$i++){
            if($size == $arr[$i]){
                return false;
            }
            
        }
        return true;
    }



}
