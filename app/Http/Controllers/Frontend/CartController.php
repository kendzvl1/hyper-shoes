<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Services\ProductService;
use App\Services\PayService;
class CartController extends Controller
{
    protected $productService;
    protected $payService;
    public function __construct(ProductService $productService , PayService $payService){
        $this->productService = $productService;
        $this->payService = $payService;
    }
	public function index()
	{
        $product = Session::get('carts') ?? collect();
		return view('frontend.carts.index',[
            'products'=>$product,
        ]);
	}

    public function store(Request $request)
    {
      try
      {
        
        $product = $this->productService->findById($request->id);
        $cart = Session::get('carts') ?? collect();
        $productInCart = $cart->where('id',$request->id)->first();
        if($cart->count() === 0 || !$productInCart){
           // dd($productInCart);
           
           $product['qty']=$request->amount ;
        }else{
            $productInCart['qty'] = $productInCart['qty']+$request->amount ;
      
        }
        $product['size']=$request->size;
        //dd($product);
        $cart->push($product);
        Session::put('carts',$cart);

        Session::flash('message','Thêm vào giỏ hàng thành công');

        return \redirect()->back();

         
     

        }catch(\Exception $e){
            report($e);
            abort(500);
        }
   
    }

    public function update(Request $request)
    {
        $cart = Session::get('carts') ?? collect();
        if($cart->count()){
            foreach($request->qty as $productID=>$qty){
                if($qty <=0 || !\is_numeric($qty)){
                    continue;
                }
                $productInCart = $cart->where('id',$productID)->first();
                $productInCart['qty'] = $qty;
            }
        }
        return \redirect(route('cart.index'));
    }

    public function destroy($id)
    {
        $cart = Session::get('carts') ?? collect();
        $products = $cart->reject(function($product) use($id){
            return $product->id == $id;
        });

        Session::put('carts',$products);
        return \redirect(route('cart.index'));
    }

    public function paymoney(Request $request){

   
        $order = $this->payService->createOrder($request);
        
        $test = $this->payService->oderDetail($order);

        
        Session::forget('carts');
        return \redirect(route('orders'));
       
     
    }
}
