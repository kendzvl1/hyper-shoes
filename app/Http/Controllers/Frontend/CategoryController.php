<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\CategoryService;
use App\Models\Size;
class CategoryController extends Controller
{
	protected $categoryService;

	public function __construct(CategoryService $categoryService)
	{
		$this->categoryService = $categoryService;
	}

    public function index(Request $request, $slug)
    {
    	$category = $this->categoryService->findBySlug($slug);

       $productPaginate = $this->categoryService->getProducts($category);
		
	   $sizes = Size::all();

    	return view('frontend.categories.index', [
    		'category' 		  => $this->categoryService->findBySlug($slug),
			'productPaginate' => $productPaginate,
			'sizes'=>$sizes
    	]);
    }
}
