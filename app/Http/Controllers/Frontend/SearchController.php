<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Size;
use App\Support\Collection;
class SearchController extends Controller
{
    public function indexSearch(Request $request){


        $products = Product::where('name','like',"%" .$request->keyword. "%")->paginate(12);
        $products->appends(['keyword'=> $request->keyword]);
     //   dd($products);
       
      
        return view('frontend.search.index',[
            'products'=>$products,
            
        ]);
    }

  

    public function categorySearch(Request $request){
        //dd($request->all());
        $sizes = Size::all();
        if($request->size == "Size"){
            $request->size=40;
        }
        if($request->range == "Khoảng giá"){
            $request->range=1;
        }
        $begin=0;
        $atter=0;
        if($request->range == 1){
            
           $atter=3000000;
        }

        if($request->range == 2){
            $begin=3000000;
            $atter=5000000;
         }

         if($request->range == 3){
            
            $begin=5000000;
            $atter=10000000;
         }
        
         $products = $this->sortRange($request->size,$begin,$atter,$request->sort,$request->range);
     
        if($products=='error'){
        
        }else{
           // dd($products->count());
            return view('frontend.search.index',[
                'products'=>$products,
                
            ]);
        }


    }

    public function sortRange($size,$begin,$atter,$type,$range){
      
        $query = Size::where(['size'=>$size])->first();

       
        if($query == null){
            return "error";
        }else{

            if($type == 'desc'){
                $product = $query->products->where('price','>=',$begin)
                ->where('price','<=',$atter)->sortByDesc('price');
               }else{
                 $product = $query->products->where('price','>=',$begin)
                 ->where('price','<=',$atter)->sortBy('price');
               }
         
        //       dd($product);
             $collection = (new Collection($product))->paginate(20);
             $collection=$collection->appends(['size'=> $size,
                                    'range'=>$range,
                                    'sort'=>$type
             ]);
           //  dd($collection);
             return $collection;
        }
      
     
       // dd($product);

    }
}
