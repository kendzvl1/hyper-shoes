<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use App\Services\SlideService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
	protected $productService;
	protected $slideService;

	public function __construct(ProductService $productService, SlideService $slideService)
	{
		$this->productService = $productService;
		$this->slideService = $slideService;
	}

    public function index(Request $request)
    {
		
		//dd($request->keyword);
		 $slides = $this->slideService->getSlide();
		 $products = $this->productService->getNewest();
    	// $products = ($request->keyword || $request->category_id) ? $this->productService->search($request) : $this->productService->getNewest();
		// $sale =$this->productService->productSale(2);
		
    	return view('frontend.home.index', [
			'products' => $products,
			'slides'=>$slides,
	//		'sale'=>$sale,
		
    	]);
    }
}
