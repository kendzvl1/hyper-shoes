<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    //
    public $timestamps = false;
    protected $hidden = ['pivot'];
    protected $guarded =[];
    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
