<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    

    public function categories(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function images()
    {
    	return $this->hasMany(ProductImage::class);
    }
  
    public function sizes(){
        return $this->belongsToMany(Size::class);
    }

    public function orders(){
        return $this->belongsToMany(Order::class);
    }
}
