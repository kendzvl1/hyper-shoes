<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Oder;

class ProductService
{
    const PER_PAGE = 12;

    public function findById($id){
        return Product::find($id);
    }

    public function findBySlug($slug)
    {
        return Product::where(['slug' => $slug])->first();
    }


    public function getNewest()
    {
        
        return Product::where('delete','=',0)->orderBy('id', 'DESC')->take(self::PER_PAGE)->get();
    }

   

    public function productSale($limit=null){
        $query = Product::inRandomOrder()->take($limit)->get();
      
        
        return $query;
    }

    public function productMin($limit=null){

        $query = Product::orderBy('price','asc')->take($limit)->get();
        return $query;
    }

   




}