<?php

namespace App\Services;

use App\Models\Slide;

class SlideService
{

    public function getSlide(){

        $slide =  Slide::with('category')->orderBy('slot','asc')->get();
      //  dd($slide);
        return $slide;
    }

}