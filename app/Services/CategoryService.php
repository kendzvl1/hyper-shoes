<?php

namespace App\Services;

use App\Models\Category;

class CategoryService
{
    const PER_PAGE = 12;

    public function getAll()
    {
        return Category::get();
    }

    public function menus()
    {
        return Category::where('delete','=',0)
        ->get();
    }

    public function findBySlug($slug)
    {
        return Category::where(['slug' => $slug])->first();
    }

    public function getProducts(Category $category)
    {
        return $category->products()->where('delete','=',0)->paginate(self::PER_PAGE);
    }

 

    public function search(Category $category, $keyword = null)
    {
        $query = $category->products();

        if ($keyword) {
            $query->where('name', 'like', "%" . $keyword . "%");
        }

        return $query->paginate(self::PER_PAGE);
    }
    public function getPhukien($limit =null){
        
        $query = Category::find(5)->products()->orderBy('name')->take($limit)->get();
        return $query;
    }
}