<style>
    input[type='text'] {    
        border: navajowhite;
        padding: 10px;
        min-width: 350px;
        border-radius: 5px;
    }

    select {
        min-width: 350px;
    }
</style>

<form action="" method="">
    <div class="filter">
        <div class="item">
            <input type="text" name="keyword" placeholder="Tên sản phẩm" value="{{ request('keyword') }}">
        </div>
        <div class="item">
            <select name="category_id" @if(!empty($categoryId)) disabled @endif>
                <option value="">Chọn danh mục sản phẩm</option>
                @foreach ($categories as $category)
                    @php
                        $selected = $category->id == $categoryId ? 'selected' : '';
                    @endphp
                    <option {{ $selected }} value="{{ $category->id }}">
                        {{ $category->name }}
                    </option>
                @endforeach
            </select>

            @if(!empty($categoryId))
                <input type="hidden" name="category_id" value="{{ $categoryId }}">
            @endif
        </div>
        <div class="item">
            <button>Tìm kiếm</button>
        </div>
    </div>
</form>