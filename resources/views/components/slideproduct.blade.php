<slideproduct>

<div class="slideproduct">
<div class="container">
<div class="owl-carousel ">
		      
          
          @foreach($sale as $sal)
           <div class="items"> 
              <a href="{{url($sal->slug)}}/product">
              <div class="ps-sale">
                  <span>{{$sal->discount}} HOT</span>
              </div>
            <img src="{{url($sal->image)}}" alt="">
            <h2>{{$sal->name}}</h2>
            <h3><span> {{number_format($sal->price,0)}}</span> VNĐ</h3>
            </a>
          </div>
		  
          @endforeach
</div>
</div>
</div>
</slideproduct>
