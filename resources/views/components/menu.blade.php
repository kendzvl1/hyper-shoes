<div>
    <ul class="parent owl-carousel  ">
    	@foreach($categories as $category)
        	<li>
        		<a href="{{ url($category->slug) }}" title="{{ $category->name }}">
        			{{ $category->name }}
				</a>
				
        	</li>
        @endforeach
    </ul>
</div>
