<accessories>


<div class="accessories">
<div class="container">
<div class="owl-carousel ">
		   
       @foreach($pk as $pkdetail)
          
          <div class="items"> 
          <a href="{{url($pkdetail->slug)}}/product">
            <img src="{{url($pkdetail->image)}}" alt="">
            <h2>{{$pkdetail->name}}</h2>
            <h3><span>{{number_format($pkdetail->price,0)}}</span> VNĐ</h3>
            </a>
          </div>
  @endforeach

          
          
</div>
</div>
</div>
</accessories>
