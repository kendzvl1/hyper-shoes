<policy>
<div class="shipping">
    <div class="container">
    	{{ $slot ?? null }}

        <div class="row">
            <div class="item">
                
                <div class="if">
                <i class="far fa-copyright"></i>
                 <h3>cam kết chính hãng</h3>
                  
                    <p>100% Authentic</p>
                    {{ $freeShipContent }}
                </div>
            </div>
            <div class="item">
               
                <div class="if">
                <i class="fal fa-truck"></i>
                <h3>giao hàng hỏa tốc</h3>
                    <p>Express delivery</p>
                    {{ $freeReturnContent }}
                </div>
            </div>
            <div class="item">
                
                <div class="if">
                <i class="fal fa-lock-alt"></i>
                <h3>hỗ trợ 24/24</h3>
                    <p>Supporting 24/24</p>
                    {{ $secureContent }}
                </div>
            </div>
        
        </div>
    </div>
    </div>
</policy>