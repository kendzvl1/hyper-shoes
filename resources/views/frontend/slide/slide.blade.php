

@php 
$dem=0;

@endphp
<div class="slidex">



<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

<div class="row">


<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
  @foreach($slides as $sl)
    @if($dem==0)
      <div class="carousel-item active">
      <a href="{{$sl->category->slug}}">
        <img class="d-block w-100" src="{{url($sl->image)}}" alt="First slide">
      </a>
    </div>
    @php
    $dem=$dem+1;
    @endphp
    @else
    <div class="carousel-item">
      <a href="{{$sl->category->slug}}">
        <img class="d-block w-100" src="{{url($sl->image)}}" alt="First slide">
      </a>
    </div>

    @endif
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    
</div>
</div>
</div>