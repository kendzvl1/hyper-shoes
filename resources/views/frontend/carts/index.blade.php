@extends('layouts.frontend.master')

@section('page_title', 'Giỏ hàng')

@section('main_content')
<list-product>
    <div class="container">
        <div class="titlePrimary">
            GIỎ HÀNG CỦA BẠN 
        </div>
      
        @if($products->count() ==0)
        <div class="thongbaox text-center">
            <img style="margin: 0 auto" src="images/tiki.png" alt="">
            <p style="font-size: 20px">Không có sản phẩm nào trong giỏ hàng của bạn.</p>
            </div>
        @else

        <br>
        <div class="giohang">
        <div class="row">
        <form action="{{route('cart.update')}}" method="post" style="width:100%">
        @csrf
        @method('put')
            <table width="100%"  class="table table-bordered" cellspacing="1px" cellpadding="5px" style="color:#000000;background:#ECEAEA;width:100%;">
                <tr class="tr_giohang" bgcolor="#535353"  height="45" align="center" style="
    font-weight: bold;
">

                    <td>#ID</td>
                    <td>Tên sản phẩm</td>
                    <td>Đơn giá</td>
                    <td>Số lượng</td>
                    <td>Thành tiền</td>
                    <td>Action</td>
             
                
                </tr>
                @php 
                    $total=0;
                @endphp

                @foreach($products as $product)

                @php 
                    $total +=$product->price * $product->qty;
                @endphp

                <tr bgcolor="#FFFFFF" height="45" align="center" style="font-size: 16px;text-align: center;">

                    <td>{{$product->id}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{number_format($product->price ,0)}} </td>
                    <td width="100"><input type="number" name="qty[{{$product->id}}]" value="{{$product->qty}}"></td>
                    <td>{{number_format ($product->price * $product->qty,0)}}</td>
                    <td width="60" align="center"><a href="{{route('cart.delete',['id'=>$product->id])}}"><i class="far fa-trash-alt"></i></a></td>
                
                </tr>

                @endforeach
                <tr>
                    <td colspan="4" align="right"><button class="btn btn-info" type="submit" >Cập nhật</button> </td>
                    <td height="30" align="center" colspan="6">
                        Tổng tiền : {{number_format($total,0) }} VNĐ
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="6"><a class="tienhanh" href="{{route('confirm')}}">Tiến hành đặt hàng</a></td>
                </tr>
            
            </table>
            </form>
        </div>
        </div>

        @endif
    


        
  
        
    </div>
</list-product>
@endsection