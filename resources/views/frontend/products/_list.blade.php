<list-product>
    <div class="container">
        <!-- <div class="titlePrimary">
            {{ $category->name ?? 'Top month Sellers' }}
        </div> -->

        <!-- <x-search category-id="{{ $category->id ?? null }}"></x-search> -->

        @php
            $products = !empty($productPaginate) ? $productPaginate : $products;
            $check = !empty($new) ? true : false;
        @endphp
        
        @if($products->count() === 0 )
        <div class="alert alert-warning" role="alert">
        Không tìm thấy sản phẩm nào phù hợp
        </div>
        @endif

        <div class="__product">
            @foreach($products as $product)
                <div class="item">
                @if($check)
                    <div class="ps-badge">
                        <span>New</span>
                    </div>  
                @endif

                
                    <div class="image">
                        <a href="{{ url($product->slug) }}/product">
                            <img src="{{url($product->image)}}" alt="">
                 
                    </div>
                    <h2>{{ $product->name }}</h2>
                    <h2>
                        
                       
                       
                       <span>{{ number_format($product->price,0) }} VNĐ</span>
                      

                    </h2>
                    </a>
                </div>
            @endforeach
        </div>

        @if (!empty($productPaginate))
            <div style="    margin: 30px 25%;">{{ $productPaginate->links() }}</div>
        @endif
    </div>

</list-product>