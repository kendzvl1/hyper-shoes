@extends('layouts.frontend.master')

@section('page_title', $product->name ?? $product->name)




@php
  $dem=0;
@endphp
@section('main_content')
<link href="{{url('css/style2.css')}}" rel="stylesheet" type="text/css">

<div class="container"> 

  <div class="container-fliud"> 
   <div class="wrapper row"> 
    <div class="preview col-md-6"> 
     <div class="preview-pic tab-content"> 

@foreach($product->images as $img)
@php
  $dem=$dem+1;
  @endphp
  @if($dem==1)
  <div class="tab-pane active" id="pic-1"><img src="{{url($img->image)}}" alt="">
      </div> 
@else
     
      <div class="tab-pane" id="pic-{{$dem}}"><img src="{{url($img->image)}}" alt="">
      </div> 
     

      @endif
@endforeach

     </div> 

     <ul class="preview-thumbnail nav nav-tabs">
     	
@php
  $dem=0;
@endphp
     @foreach($product->images as $img)
       
     @php
      $dem=$dem+1;
    @endphp
      <li><a data-target="#pic-{{$dem}}" data-toggle="tab"><img src="{{url($img->image)}}" alt=""></a>
      </li> 
     
  @endforeach

     </ul> 
    </div> 
    
    <div class="details col-md-6"> 
     <h3 class="product-title">{{$product->name}}</h3> 
     <div class="rating"> 
      <div class="stars"> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> 
     
   
     @if($product->discount > 0)
     </div> <span class="review-no">Khuyễn mãi {{$product->discount}} %</span> 
     </div> 
     <h4 class="price">  {{number_format($product->price - $product->price*($product->discount)/100,0) }} VNĐ <s>{{number_format($product->price,0)}} VNĐ</s></h4> 
    @else
    <h4 class="price"> {{number_format($product->price)}} VNĐ</h4> 
     @endif

     <!-- <p class="vote"><strong>91%</strong> of người mua hài lòng với sản phẩm này <strong>(87 bình chọn)</strong> -->
     </p> 
     <form action="{{route('cart.store')}}" method="get">
     <input type="hidden" name="id" value="{{$product->id}}" >
     <h5 class="sizes">Size :
          <div class="form-check form-check-inline">

       
          @foreach($product->sizes as $sz)

         

           <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" id="size" name="size"  value="{{$sz->size}}" required>
            <label class="form-check-label" for="inlineRadio1">{{$sz->size}}</label>
          </div>

          @endforeach

         

          </div>
          
     </h5> 
     <h5 class="colorS">Số lượng:
     <p></p>
     <div class="increa">
     <button id="tru" type="button">-</button>
            <div class="form-group">
          <input type="text" class="form-control" id="soluong" name="amount" value="1">
        </div>
        <button id="cong" type="button">+</button>
        </div>
     </h5> 
     <div class="action">  
      <button class="add-to-cart btn btn-default" type="submit" >THÊM VÀO GIỎ HÀNG</button>          
     </div> 
   
     </form>
     @if(Session::has('message'))
     <div class="alert alert-success text-center m-5">
      {{Session::get('message')}}
     </div>
     @endif
    </div> 
   </div> 
  </div> 
  <x-title title="- có thể bạn quan tâm"></x-title>
  <div class="sua">
  <x-slideproduct :limit=25></x-slideproduct>
  </div>

</div> 

</div> 
</div> 
</div> 
<script>




var sl= parseInt($('#soluong').val());

$('#cong').click(function(){
if(sl<10){
  sl=sl+1;
  $('#soluong').val(sl);
}  
});
  $('#tru').click(function(){
    if(sl>1){
      sl=sl-1;
      $('#soluong').val(sl);
    }
  });
  


 
</script>


@endsection